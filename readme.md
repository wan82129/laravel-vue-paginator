Usage

```
<template>
    <PaginateComponent :data="events" />
</template>
import PaginateComponent from '@/Layouts/PaginatorComponent.vue';

export default {
    components: {
        PaginateComponent,
    }
};
</script>
```